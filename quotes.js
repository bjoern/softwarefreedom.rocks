
window.onload = function () {
    var count = -1;
    var quotes = [
	{'author':'Richard M. Stallman', 'quote':'I could have made money this way [writing proprietary software], and perhaps amused myself writing code. But I knew that at the end of my career, I would look back on years of building walls to divide people, and feel I had spent my life making the world a worse place.'},
	{'author':'Richard M. Stallman', 'quote':'Geeks like to think that they can ignore politics, you can leave politics alone, but politics won\'t leave you alone.'},
	{'author':'', 'quote':'Many small people who in many small places do many small things that can alter the face of the world.'},
	{'author':'Richard M. Stallman', 'quote':'Free Software is a matter of liberty, not price. To understand the concept, you should think of \'free\' as in free speech, not as in free beer.'},
	{'author':'Richard M. Stallman', 'quote':'Playfully doing something difficult, whether useful or not, that is hacking.'},
	{'author':'Richard M. Stallman', 'quote':'All intellectual property rights are just licenses granted by society because it was thought, rightly or wrongly, that society as a whole would benefit by granting them. But in any particular situation, we have to ask: are we really better off granting such license? What kind of act are we licensing a person to do?'},
	{'author':'Richard M. Stallman', 'quote':'If you want to accomplish something in the world, idealism is not enough — you need to choose a method that works to achieve the goal. In other words, you need to be pragmatic.'},
	{'author':'Richard M. Stallman', 'quote':'Value your freedom or you will lose it, teaches history. "Don\'t bother us with politics," respond those who don\'t want to learn.'},
	{'author':'Richard M. Stallman', 'quote':'Isn\'t it ironic that the proprietary software developers call us communists? We are the ones who have provided for a free market, where they allow only monopoly.'},
	{'author':'Eben Moglen', 'quote':'The great moral question of the 21st century is this: if all knowing, all culture, all art, all useful information can be costlessly given to everyone at the same price that it is given to anyone; if everyone can have everything, anywhere, all the time, why is it ever moral to exclude anyone?'},
	{'author':'Eben Moglen', 'quote':'Everybody is connected to everybody else, all data that can be shared will be shared: get used to it.'},
	{'author':'Eben Moglen', 'quote':'The more we give away, the richer we become.'},
	{'author':'Lawrence Lessig', 'quote':'If the Internet teaches us anything, it is that great value comes from leaving core resources in a commons, where they\'re free for people to build upon as they see fit.'},
	{'author':'Eben Moglen', 'quote':'Licenses are constitutions for communities'},
	{'author':'Lawrence Lessig', 'quote':'Technology means you can now do amazing things easily; but you couldn\'t easily do them legally.'},
	{'author':'Victor Hugo', 'quote':'Before the publication, the author has an undeniable and unlimited right. Think of a man like Dante, Molière, Shakespeare. Imagine him at the time when he has just finished a great work. His manuscript is there, in front of him; suppose that he gets the idea to throw it into the fire; nobody can stop him. Shakespeare can destroy Hamlet, Molière Tartufe, Dante the Hell. But as soon as the work is published, the author is not any more the master. It is then that other persons seize it: call them what you will: human spirit, public domain, society. It is such persons who say: I am here; I take this work, I do with it what I believe I have to do, [...] I possess it, it is with me from now on...'},
	{'author':'Georg Greve', 'quote':'At the beginning, every software was free.'},
    ];

    function shuffle(a) {
	var j, x, i;
	for (i = a.length - 1; i > 0; i--) {
	    j = Math.floor(Math.random() * (i + 1));
	    x = a[i];
	    a[i] = a[j];
	    a[j] = x;
	}
	return a;
    }

    quotes = shuffle(quotes);

    function rotateQuotes() {
	count = count + 1;
	if (count >= quotes.length) {
	    count = 0;
	} else if (count < 0) {
	    count = quotes.length - 1;
	}
	var newQuote = '<blockquote>' + quotes[count]['quote'] + ' <cite>' + quotes[count]['author'] + '</cite></blockquote';
	document.getElementById('quote').innerHTML = newQuote;
    }

    setTimeout(function() {
	rotateQuotes();
	setInterval(function() { rotateQuotes(); }, 10000);	
    }, 3000);
    
    
}
